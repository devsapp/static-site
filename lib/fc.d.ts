import { InputProps } from './entity';
export default class FunctionCompute {
    protected inputs: InputProps;
    protected client: any;
    constructor(inputs: InputProps);
    private sendHttpRequest;
    private makeFcUtilsFunctionTmpDomainToken;
    private deleteFcUtilsFunctionTmpDomain;
    private processTemporaryDomain;
    createService(serviceName: string): Promise<string>;
    createOrUpdateFunction(serviceName: string, functionName: string, zipFile: any): Promise<void>;
    createTrigger(serviceName: string, functionName: string, triggerName: string): Promise<void>;
    private getCustomDomain;
    private updateCustomerDomain;
    private createCustomDomain;
    private listCustomDomains;
    checkDomainHasBindFunction(serviceName: any, functionName: any): Promise<any>;
    createOrUpdateCustomDomain(domainName: string, options: any): Promise<{}>;
    deployByImage(imageUrl: string): Promise<unknown>;
    deployByCode(codeBase64: string): Promise<unknown>;
}
