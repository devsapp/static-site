export default class BaseComponent {
    protected inputs: any;
    protected client: any;
    private name;
    constructor(inputs: any);
    __doc(projectName?: string): string;
    protected __report(reportData: ServerlessDevsReport.ReportData): ServerlessDevsReport.Domain | ServerlessDevsReport.Fc | ServerlessDevsReport.Oss | ServerlessDevsReport.Ram | ServerlessDevsReport.Sls | ServerlessDevsReport.ApiGw | ServerlessDevsReport.CDN | ServerlessDevsReport.Vpc | ServerlessDevsReport.Fnf | ServerlessDevsReport.Cr | ServerlessDevsReport.Sae;
}
