import BaseComponent from './base';
import { InputProps } from './entity';
export default class ComponentDemo extends BaseComponent {
    constructor(props: any);
    private exeBuildWebStaticCmd;
    private transformStaticDirToReleaseDir;
    private writeBootstrapFile;
    private installHttpserver;
    /**
     * 测试函数
     * @param inputs
     */
    test(inputs: InputProps): Promise<void>;
    /**
     * 启动服务，如果是源码则执行源码路径的npm start, 如果只是静态目录则启动http服务
     * @param inputs
     */
    serve(inputs: InputProps): Promise<void>;
    /**
     * 构建部署 可以帮助用户把静态站点托管到fc，支持任意前端框架
     * @param inputs
     * @returns
     */
    deploy(inputs: InputProps): Promise<any>;
}
