

import path from 'path';
import fs from 'fs-extra';
import { spawn } from 'child_process';
import get from 'lodash.get';
import { zip } from '@serverless-devs/core';
import BaseComponent from './base';
import logger from './logger';
import { InputProps } from './entity';
import FunctionCompute from './fc';


export default class ComponentDemo extends BaseComponent {
  constructor(props) {
    super(props)
  }

  private async exeBuildWebStaticCmd(_path: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (fs.existsSync(path.join(_path, 'package.json'))) {
        const buildCmd = spawn('npm', ['run', 'build'], {
          shell: true,
          cwd: _path,
          stdio: 'inherit'
        });
        buildCmd.on('close', (code) => {
          logger.success('execute build successfuly');
          resolve({ code });
        });
      } else {
        resolve({});
      }
    })
  }

  private transformStaticDirToReleaseDir(source: string, target: string) {
    const staticFoldName = path.basename(source);
    const targetReleaseStaticDir = path.join(target, staticFoldName);
    fs.ensureDirSync(targetReleaseStaticDir);
    fs.copySync(source, targetReleaseStaticDir);
    return staticFoldName;
  }

  private writeBootstrapFile(releaseDir, staticFoldName, port) {
    const bootstrapFile = path.join(releaseDir, 'bootstrap');
    const bootStrapContent = `#!/usr/bin/env bash\n\nexport PORT=${port}\nnpx http-server ${staticFoldName} -p $PORT`;
    fs.writeFile(bootstrapFile, bootStrapContent, { mode: '777', encoding: 'utf8' });
  }

  private installHttpserver(releaseDir) {
    const packageContent = `{
      "name": "webstatic",
      "version": "1.0.0",
      "description": "",
      "main": "index.js",
      "author": "",
      "license": "ISC",
      "dependencies": {
        "http-server": "^0.12.3"
      }
    }
    `;
    fs.writeFile(path.join(releaseDir, 'package.json'), packageContent, { mode: '777', encoding: 'utf8' });

    return new Promise((resolve, reject) => {
      const buildCmd = spawn('npm', ['install'], {
        shell: true,
        cwd: releaseDir,
        stdio: 'inherit'
      });
      buildCmd.on('close', (code) => {
        resolve({ code });
      });
    })
  }
  /**
   * 测试函数
   * @param inputs 
   */
  public async test(inputs: InputProps) {
    console.log('test project')
  }
  /**
   * 启动服务，如果是源码则执行源码路径的npm start, 如果只是静态目录则启动http服务
   * @param inputs 
   */
  public async serve(inputs: InputProps) {
    const sourceCode = get(inputs, 'props.sourceCode');
    const targetStaticDir = get(inputs, 'props.targetStaticDir');
    if (fs.existsSync(sourceCode)) {
      spawn('npm', ['start'], {
        shell: true,
        cwd: sourceCode,
        stdio: 'inherit'
      });

    } else if (fs.existsSync(targetStaticDir)) {
      spawn('npx', ['http-server'], {
        shell: true,
        cwd: targetStaticDir,
        stdio: 'inherit'
      });
    }
    else {
      logger.info('没有发现源码路径下有可执行的pakcage.json，请确保源码路径有package.json，并且请配置 start 指令')
    }

  }

  /**
   * 构建部署 可以帮助用户把静态站点托管到fc，支持任意前端框架
   * @param inputs
   * @returns
   */
  public async deploy(inputs: InputProps) {
    const sourceCode = get(inputs, 'props.sourceCode');
    if (fs.existsSync(sourceCode)) {
      // 进行构建
      await this.exeBuildWebStaticCmd(sourceCode);
    }
    const targetStaticDir = get(inputs, 'props.targetStaticDir'); // 
    if (!fs.existsSync(targetStaticDir)) {
      throw new Error('指定静态目录不存在，请先设置要发布的静态目录')
    }
    const releaseFolderName = '_fc_release';
    const releaseDir = path.join(process.cwd(), releaseFolderName);
    if (fs.existsSync(releaseDir)) {
      fs.removeSync(releaseDir);
    }
    fs.ensureDirSync(releaseDir);
    const port = get(inputs, 'props.port', 9000);
    const staticFoldName = this.transformStaticDirToReleaseDir(targetStaticDir, releaseDir);
    this.writeBootstrapFile(releaseDir, staticFoldName, port);
    await this.installHttpserver(releaseDir);
    const fc = new FunctionCompute(inputs);

    const zipFilePath = `${path.join(process.cwd(), releaseFolderName)}.zip`;
    await zip({ codeUri: releaseDir, outputFileName: path.basename(releaseDir) });

    let data = fs.readFileSync(zipFilePath);
    data = Buffer.from(data, 'utf-8').toString('base64');
    const deployResult: any = await fc.deployByCode(data);
    fs.unlinkSync(zipFilePath);
    if (deployResult.domain) {
      super.__report({
        name: 'domain',
        access: inputs.project.access,
        content: {
          domain: deployResult.domain,
          weight: 1
        }
      });
    }
    if (deployResult.function) {
      const { service, function: functionName, trigger, region } = deployResult;
      super.__report({
        name: 'fc',
        access: inputs.project.access,
        content: { service, function: functionName, trigger, region }
      })
    }
    return deployResult
  }

}
