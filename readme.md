## 概念说明

### 应用 application
应用 application 是serverles devs cli 的启动对象，包含全局变量,以及服务。 [具体内容查看官方文档](https://github.com/Serverless-Devs/docs/blob/master/zh/yaml.md)

### 组件 component
component 是 serverless devs 执行的主逻辑单元，用户可以在组件里自定义不同的流程处理逻辑。 一个component 可看做是一个npm 包 (类似 java jar)
## 使用说明
本项目是开发 Serverless devs 组件component 的标准模板，提供了日志打印，国际化，以及 文档生成样例。



## 使用步骤

### 1 安装依赖
```
npm i 
```
### 2 执行编译
```
npm run build
```
### 3 执行测试


```
cd example && s test
```

